use clap::Parser;
use csv::ReaderBuilder;
use mtg2influx::MTGCollection;
use std::fs;

#[derive(Parser)]
#[clap(
    name = "mt
g2influx",
    version = "0.1.0",
    author = "Romain Forlot"
)]
#[clap(
    about = "Take your cards collection then tracks and sends their prices to an influxDB through a HTTP Request.",
    propagate_version = true
)]

struct Mtg2InfluxCmd {
    /// Increase verbosity
    #[clap(long, short = 'v', default_value_t = false)]
    verbose: bool,
    /// Collection file in CSV format.
    #[clap(long, short = 'c')]
    collection_file: String,
    /// Specify where is the MTG2json AllPrices file
    #[clap(long, default_value_t = String::from("AllIdentifiers.json"))]
    identifiers_file: String,
    /// Specify where is the MTG2json AllIdentifiers file
    #[clap(long, default_value_t = String::from("AllPrices.json"))]
    prices_file: String,
}

fn main() {
    let args = Mtg2InfluxCmd::parse();
    let ids_file = args.identifiers_file;
    let prices_file = args.prices_file;
    let collec_file = args.collection_file;

    let csv = ReaderBuilder::new()
        .from_path(&collec_file)
        .unwrap_or_else(|e| panic!("{}: {}", e, collec_file));

    let ids = fs::read_to_string(&ids_file).unwrap();
    let jids = json::parse(ids.as_str()).unwrap();

    let prices = fs::read_to_string(&prices_file).unwrap();
    let jprices = json::parse(prices.as_str()).unwrap();

    let mut collection = MTGCollection::new(csv);
    let allprices = collection
        .cards_prices(&jids, &jprices, args.verbose)
        .unwrap();
    for i in allprices {
        print!("{}", i.as_lineprotocol());
    }
}
