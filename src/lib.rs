mod influx;

use chrono::NaiveDateTime;
use csv::Reader;
use json::JsonValue;
use std::fs::File;
extern crate json;
use std::fs;
use std::io::Write;

const HOMEDIR: &str = std::env!("HOME");

struct CardPriceHistory {
    timestamp: i64,
    price: f32,
}

pub struct MTGCard {
    name: String,
    setcode: String,
    edition: String,
    foil: i32,
    quantity: i32,
    prices: Vec<CardPriceHistory>,
}

impl MTGCard {
    pub fn as_lineprotocol(&self) -> String {
        let mut line: String = String::new();
        for price in &self.prices {
            line.push_str(
                format!(
                    "\"{}\" setcode={},edition=\"{}\",foil={} price={} {}\n",
                    self.name, self.setcode, self.edition, self.foil, price.price, price.timestamp,
                )
                .as_str(),
            )
        }
        line
    }

    fn deserialize_prices(prices: &JsonValue) -> Vec<CardPriceHistory> {
        let mut ret = Vec::new();

        for p in prices.entries() {
            let dt = format!("{} 12:00:00", p.0);
            let date_time = NaiveDateTime::parse_from_str(&dt, "%Y-%m-%d %H:%M:%S")
                .expect(format!("Problem computing timestamp from '{}'", p.0).as_str());
            let card_price = CardPriceHistory {
                timestamp: date_time.timestamp_nanos(),
                price: p.1.as_f32().unwrap(),
            };
            ret.push(card_price);
        }

        ret
    }

    fn most_recent_price(&self) -> i64 {
        self.prices.last().unwrap().timestamp
    }
}

pub struct MTGCollection {
    collection: Reader<File>,
    cards: Vec<MTGCard>,
}

impl MTGCollection {
    pub fn new(file: Reader<File>) -> Self {
        MTGCollection {
            collection: file,
            cards: Vec::new(),
        }
    }

    fn get_prices_from_uuid(uuid: &JsonValue, prices: &JsonValue) -> Result<JsonValue, String> {
        for price in prices["data"].entries() {
            if price.0 == *uuid {
                return Ok(price.1["paper"]["cardmarket"]["retail"]["normal"].clone());
            }
        }

        return Err(format!("No prices found for the card"));
    }

    fn get_id_from_name(
        cardname: &String,
        setcode: &String,
        jids: &JsonValue,
    ) -> Result<JsonValue, String> {
        for jid in jids["data"].entries() {
            if jid.1["name"].as_str().unwrap() == cardname
                && jid.1["setCode"].as_str().unwrap() == setcode
            {
                return Ok(jid.1["uuid"].clone());
            }
        }

        return Err(format!(
            "No id found for the card '{}' setcode '{}'",
            cardname, setcode
        ));
    }

    pub fn cards_prices(
        &mut self,
        jids: &JsonValue,
        jprices: &JsonValue,
        verbose: bool,
    ) -> Result<Vec<MTGCard>, String> {
        let mut ret: Vec<MTGCard> = Vec::new();
        let mut i = 0;
        let mut total = 0;

        for elt in self.collection.records() {
            total += 1;
            let last_record_file = fs::OpenOptions::new()
                .append(true)
                .create(true)
                .open(format!("{}/.local/mtg2influx.log", HOMEDIR))
                .expect("Can't write in ~/.local");
            let record = elt.unwrap();
            let cardname = record.get(0).unwrap_or("NoName").to_string();
            let setcode = record.get(1).unwrap_or("Nosetcode").to_string();
            let edition = record.get(2).unwrap_or("NoEdition").to_string();
            let foil: i32 = record.get(3).unwrap_or("0").parse().unwrap_or(0);
            let quantity: i32 = record.get(4).unwrap_or("0").parse().unwrap_or(0);

            let uuid = MTGCollection::get_id_from_name(&cardname, &setcode, &jids)?;
            if verbose {
                print!("'{}' uuid is {}\n", cardname, uuid);
            }
            let p = MTGCollection::get_prices_from_uuid(&uuid, &jprices);
            match p {
                Ok(val) => {
                    if verbose {
                        print!("Price for {} are {}\n", cardname, val);
                    }
                    let card = MTGCard {
                        name: cardname.clone(),
                        setcode: setcode,
                        edition: edition,
                        foil: foil,
                        quantity: quantity,
                        prices: MTGCard::deserialize_prices(&val),
                    };
                    let last_price = format!("{}: {}\n", cardname, card.most_recent_price());
                    ret.push(card);
                    write!(&last_record_file, "{}", last_price).expect("Can't write into homedir");
                }
                Err(e) => {
                    i += 1;
                    print!("{} '{}'\n", e, cardname);
                }
            }
        }

        print!("Found {} out of {} card prices.", total - i, total);
        Ok(ret)
    }
}
